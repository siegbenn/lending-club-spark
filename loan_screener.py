from datetime import datetime
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import StructField, StructType, StringType

# Bennett Siegel 2017

if __name__ == "__main__":

    # Setup
    conf = SparkConf().setAppName("screener")
    sc = SparkContext(conf = conf)

    # Load data samples into spark.
    raw_data = sc.textFile("./LoanStats_2016Q3.csv")

    # Remove info row and split csv lines.
    info_row = raw_data.first()
    raw_data_no_info = raw_data \
        .filter(lambda line: line != info_row) \
        .map(lambda line: line.replace('"', '')) \
        .map(lambda line: line.split(","))

    # Remove header row.
    header_row = raw_data_no_info.first()
    raw_data_no_header = raw_data_no_info \
        .filter(lambda line: line != header_row)

    # Remove malformed data.
    data_rows = raw_data_no_header \
        .filter(lambda line: line != header_row and len(line) ==
            len(header_row))

    # Create schema from header.
    fields = [StructField(field_name, StringType(), True) for field_name in header_row]
    schema = StructType(fields)

    # Create spark sql data table.
    sql = SQLContext(sc)
    data_table = sql.createDataFrame(data_rows, schema)
    data_table.registerTempTable("data_table")

    # Get median annual_inc and total_cur_bal grouped by grade.
    loan_grade_medians = sql.sql("SELECT grade, percentile_approx(annual_inc, 0.5) " \
        "AS annual_inc_median, percentile_approx(tot_cur_bal, 0.5) " \
        "AS tot_cur_bal_median FROM data_table GROUP BY grade")

    # Join data table with median table.
    data_table_with_medians = data_table.join(loan_grade_medians, data_table.grade
     == loan_grade_medians.grade)
    data_table_with_medians.printSchema()
    sql.dropTempTable("data_table")
    data_table_with_medians.registerTempTable("data_table_with_medians")

    # Query
    sql.registerFunction("convertTime", lambda x: datetime.strptime(x, "%b-%Y"))

    results = sql.sql("SELECT id, emp_title, annual_inc, annual_inc_median, " \
        "tot_cur_bal, tot_cur_bal_median, earliest_cr_line " \
        "FROM data_table_with_medians " \
        "WHERE abs(annual_inc - annual_inc_median) >= 17000 " \
        "AND abs(annual_inc - annual_inc_median) <= 18000 AND " \
        "abs(tot_cur_bal - tot_cur_bal_median) >= 6000 AND " \
        "abs(tot_cur_bal - tot_cur_bal_median) <= 7000 " \
        "ORDER BY convertTime(earliest_cr_line) ASC LIMIT 7")
    results.show()

    # Collect and return the last (7th oldest earliest_cr_line) row.
    seventh_oldest = results.collect()[-1]
    print "SEVENTH OLDEST\n==========================="
    print seventh_oldest.id, seventh_oldest.emp_title