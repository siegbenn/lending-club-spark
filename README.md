# README #

This project uses Apache Spark to crunch lending club data. This project was done as a demo for an analytics startup.

### How do I get set up? ###

* Clone the repo.
* Install Apache Spark.
* $ spark-submit loan_screener.py